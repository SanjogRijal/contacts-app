# contacts-app
Angular 2 project to pick contact details from database and install

#steps to use
1. Download the zip file or clone the project
2. Type npm install from inside the main application directory to install all the dependencies
3. Create the necessary database in mysql.
    3.a) The Database name is contacts
    3.b) Table name is user_contacts
4. Create a folder called angularapi inside your htdocs directory in xampp.
5. Copy the select.php file that I have put here

Then run the application with ng serve or npm start. You will find that the datas from database has been pulled to show contact list.

Due to lack of knowledge at present I am unable to perform Create, Update and Delete feature. However I have tried my best to perform it as you can see deleteClicked() function in Service. 


